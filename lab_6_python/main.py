from functools import reduce


def swap(data, i, j):
    """
    Вспомогательная функция перестановки двух элементов в массиве

    :param data: Одномерный массив целых чисел.
    :param i: Индекс
    :param j: Индекс
    :return: [data]
    """
    buffer = data[i]
    data[i] = data[j]
    data[j] = buffer


def support_map_fun(pair):
    """
    Вспомогательная функция-преобразования Pair['row', 'data'] в Triple['min', 'row', 'col]

    :param pair: индекс, одномерный целочисленный массив
    :return: Triple
    """
    min_value = find_min_or_none(pair['data'])
    if min_value is not None:
        return {'min': min_value['min'], 'row': pair['row'], 'col': min_value['index']}


def replace_all(data, old_value, new_value):
    """
    Заменяет все вхождения целочисленного значения в массиве [data].

    :param data: Одномерный массив целых чисел.
    :param old_value: Искомое значение.
    :param new_value: Новое значение.

    :return: [data]
    """
    for index, element in enumerate(data):
        if element == old_value:
            data[index] = new_value
    return data


def rotate_left_by(data, shift):
    """
    Осуществлять циклический сдвиг элементов одномерного массива на заданное число позиций влево.

    :param data: Одномерный массив целых чисел.
    :param shift: Сдвиг.
    :return: Новый массив.
    """
    if len(data) == 0:
        return data
    shift = shift % len(data)
    if shift < 0:
        shift += len(data)

    for i in range(shift):
        buffer = data[0]
        for j in range(len(data) - 1):
            data[j] = data[j + 1]
        data[len(data) - 1] = buffer
    return data


def reverse_order(data):
    """
    Осуществить перестановку значений элементов массива [data] в обратном порядке.

    :param data: Одномерный массив целых чисел.
    :return: [data]
    """
    avg_index = (len(data) // 2) - 1
    if avg_index < 0:
        return data

    reverse_index = len(data) - 1
    for index in range(avg_index + 1):
        swap(data, index, reverse_index)
        reverse_index = reverse_index - 1

    return data


def find_min_or_none(data):
    """
    Функция получает одномерный массив целых переменных.
    Вычисляет и возвращает минимальный по значению элемент этого массива и номер его индекса.

    :param data: Одномерный массив целых чисел.
    :return: Пара значений: минимальный по значению элемент [data] и его индекс или None.
    """
    if len(data) == 0:
        return None
    index = 0
    min_elem = data[index]
    for i in range(1, len(data)):
        e = data[i]
        if min_elem > e:
            min_elem = e
            index = i
    return {'index': index, 'min': min_elem}


def find_min_or_none_2d(data):
    """
    Отыскать минимальный элемент двумерного массива целых чисел.

    :param data: Двумерный массив целых чисел.
    :return: Минимальный по значению элемент этого массива и номера индексов или None.
    """
    indexed_data = [{'row': index, 'data': element} for index, element in enumerate(data)]
    filtered = [triple for triple in list(map(support_map_fun, indexed_data)) if triple is not None]
    if len(filtered) == 0:
        return None
    return reduce(lambda a, b: a if (a['min'] < b['min']) else b, filtered)


def binary_search(data, element):
    """
    Функция осуществляет бинарный поиск элемента в упорядоченном одномерном массиве.

    :param data: Упорядоченный одномерный массив целых чисел.
    :param element: Искомый элемент
    :return: Индекс [element] в массиве [this] или -1, если элемент не найден.
    """
    first_index = 0
    last_index = len(data) - 1
    if last_index == -1:
        return -1

    while first_index <= last_index:
        avg_index = (last_index + first_index) // 2
        if element > data[avg_index]:
            first_index = avg_index + 1
        elif element < data[avg_index]:
            last_index = avg_index - 1
        else:
            return avg_index
    return -1


def bubble_sort_asc(data):
    """
    Сортирует массив в порядке возрастания методом пузырька.

    :param data: Одномерный массив целых чисел.
    :return: [data]
    """
    is_sorted = False
    while not is_sorted:
        is_sorted = True
        for i in range(1, len(data)):
            if data[i - 1] > data[i]:
                swap(data, i - 1, i)
                is_sorted = False
    return data

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    arr = [6, -7, 0, 2, 3, 4, 5]
    print(bubble_sort_asc(arr))

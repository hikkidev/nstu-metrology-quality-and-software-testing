/**
 * Класс описывает индекс
 */
@JvmInline
value class Index(val value: Int) {
    override fun toString(): String = value.toString()

    companion object {
        /**
         * Создать массив [Index] из множества [Int]
         */
        fun arrayOf(vararg values: Int) = values.map { Index(it) }.toTypedArray()

        /**
         * Создать массив [Index] из множества [range]
         */
        fun arrayOf(range: IntRange): Array<Index> {
            val iterator = range.iterator()
            return Array(range.count()) { Index(iterator.nextInt()) }
        }
    }
}
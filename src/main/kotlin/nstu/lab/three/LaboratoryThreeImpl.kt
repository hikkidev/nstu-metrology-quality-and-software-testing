package nstu.lab.three

import countDigits
import isEven
import isOdd
import pow
import takeIf
import kotlin.math.abs


class LaboratoryThreeImpl : LaboratoryThree {

    override fun reverseOrderOfOddDigits(number: Int): Long {
        val queue = mutableListOf<Int>()
        var temp = abs(number)
        while (temp > 0) {
            val digit = temp % TEN
            if (digit.isOdd)
                queue.add(digit)
            temp /= TEN
        }
        return queue.foldIndexed(0) { index, acc, digit ->
            acc + digit * TEN.pow(queue.size - index - 1)
        }
    }

    override fun findEvenPosOfEvenDigit(number: Int): Int {
        var index = 1
        var maxValue = 0 to 0
        var temp = abs(number)
        while (temp > 0) {
            val digit = temp % TEN
            if (index.isEven && digit.isEven && digit >= maxValue.second)
                maxValue = index to digit
            temp /= TEN
            index++
        }
        return maxValue.first
    }

    override fun rotateRightBy(shift: Int, number: Int): Long {
        val allDigits = number.countDigits().toDouble()
        val digitsToShift = (shift % allDigits + allDigits) % allDigits

        // Stores last K digits of number
        val rightPart = number / TEN.pow(digitsToShift)
        val rightDigits = rightPart.countDigits().toDouble()

        // Append left_no to the right of digits of N
        return (number % TEN.pow(digitsToShift) * TEN.pow(rightDigits) + rightPart)
    }

    override fun findSumAboveSubDiagonal(matrix: Matrix): Double {
        if (matrix.maxRows < 2 || matrix.maxCols < 2) return 0.0
        return matrix.rowsIndices
            .fold(0.0) { rowAcc, row ->
                rowAcc + (0 until matrix.maxCols - row - 1)
                    .fold(0.0) { colAcc, col -> colAcc + matrix[row][col].takeIf { it.isEven } }
            }
    }

    private companion object {
        const val TEN = 10
    }
}
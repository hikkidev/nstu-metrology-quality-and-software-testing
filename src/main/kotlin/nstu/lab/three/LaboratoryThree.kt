package nstu.lab.three

/**
 * Постройте УГП и разработайте тестовые наборы данных для тестирования функций разработанных по критерию С2.
 */
interface LaboratoryThree {


    /**
     * Функция формирует и возвращает целое число из значений нечётных разрядов [number], следующих в обратном порядке
     * или 0 в противном случае.
     *
     * @param number Целое число
     *
     * Например: `reverse(132_456) -> 531`
     */
    fun reverseOrderOfOddDigits(number: Int): Long

    /**
     * Функция находит и возвращает четный разряд наибольшей чётной цифры из [number] или 0 в противном случае.
     * Разряды числа, пронумерованы справа налево, начиная с единицы.
     *
     * @param number Целое число
     *
     * Например: 54861 -> 2
     */
    fun findEvenPosOfEvenDigit(number: Int): Int

    /**
     * Функция возвращает число, полученное циклическим сдвигом значений разрядов [number]
     * на заданное число позиций [shift] вправо.
     *
     * Например, сдвиг на две позиции: `rotateRightBy(2, 1234_56) -> 56_1234`
     *
     * @param shift Сдвиг
     * @param number Целое число
     */
    fun rotateRightBy(shift: Int, number: Int): Long

    /**
     * Отыскивает и возвращает сумму чётных значений компонентов массива, лежащих выше побочной диагонали
     *
     * @param matrix Двумерный массив вещественных переменных
     */
    fun findSumAboveSubDiagonal(matrix: Matrix): Double
}
package nstu.lab.three

class Matrix constructor(val rows: Array<out DoubleArray>) : Iterable<DoubleArray> {

    val maxRows: Int
    val maxCols: Int

    val rowsIndices: IntRange get() = 0 until maxRows
    val colsIndices: IntRange get() = 0 until maxCols

    init {
        val _cols = rows.firstOrNull()?.size ?: 0
        require(rows.all { it.size == _cols }) { "Матрица должна быть прямоугольной" }
        this.maxRows = rows.size
        this.maxCols = _cols
    }

    constructor(rows: Int, cols: Int) : this(Array(rows) { DoubleArray(cols) })

    operator fun get(index: Int) = rows[index]

    override fun iterator(): Iterator<DoubleArray> = object : Iterator<DoubleArray>, ListIterator<DoubleArray> {
        private var index = 0

        override fun hasNext(): Boolean = index < maxRows

        override fun next(): DoubleArray {
            if (!hasNext()) throw NoSuchElementException()
            return get(index++)
        }

        init {
            this.index = index
        }

        override fun hasPrevious(): Boolean = index > 0

        override fun nextIndex(): Int = index

        override fun previous(): DoubleArray {
            if (!hasPrevious()) throw NoSuchElementException()
            return get(--index)
        }

        override fun previousIndex(): Int = index - 1
    }
}

fun matrixOf(vararg elements: DoubleArray) = Matrix(elements)
package nstu.lab.five

import kotlin.math.PI
import kotlin.math.log2
import kotlin.math.sqrt
import kotlin.random.Random
import kotlin.random.nextInt

class LaboratoryFiveImpl(seed: Long = System.currentTimeMillis()) : LaboratoryFive {
    private val random = Random(seed)

    override fun generateDictionary(n1: Int, n2: Int): Dictionary {
        require(n1 + n2 > 1)
        return createDict(1..(n1 + n2))
    }

    override fun programSize(n1: Int, n2: Int) = n1 * log2(n1.toDouble()) + n2 * log2(n2.toDouble())
    override fun programSize(size: Int): Double = 0.91 * size * log2(size.toDouble())
    override fun programSize(dictionary: Dictionary): Long = dictionary.values.fold(1L) { acc, frequency ->
        frequency + acc
    }

    override fun medianProgramSize(data: List<Dictionary>): Long = data.fold(0L) { acc, dictionary ->
        acc + programSize(dictionary)
    } / data.size

    override fun mean(data: List<Dictionary>): Double {
        val sizes = data.map { programSize(it) }
        val p = 1.0 / sizes.size
        return sizes.fold(0.0) { acc, size -> p * size + acc }
    }

    override fun variance(size: Int): Double = PI * PI * size * size / 6.0
    override fun variance(data: List<Dictionary>): Double = meanSquared(data) - mean(data).let { it * it }

    override fun sigma(size: Int): Double = sqrt(variance(size))
    override fun sigma(data: List<Dictionary>): Double = sqrt(variance(data))

    override fun delta(size: Int): Double = sqrt(variance(size)) / programSize(size)
    override fun delta(data: List<Dictionary>): Double = sqrt(variance(data)) / mean(data)

    private fun meanSquared(data: List<Dictionary>): Double {
        val sizes = data.map { programSize(it) }
        val p = 1.0 / sizes.size
        return sizes.fold(0.0) { acc, size -> p * (size * size) + acc }
    }

    private fun createDict(range: IntRange): Dictionary {
        val out = mutableMapOf<Key, Int>()
        range.onEach { out[it] = 0 }
        while (!out.values.all { it > 0 }) {
            val key = random.nextInt(range)
            out[key] = out[key]?.plus(1) ?: 1
        }
        return out.toMap()
    }
}
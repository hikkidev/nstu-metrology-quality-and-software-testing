package nstu.lab.five

typealias Key = Int
typealias Dictionary = Map<Key, Int>

interface LaboratoryFive {

    /**
     * Смоделировать текст программы
     *
     * @param n1 Кол-во операторов
     * @param n2 Кол-во операндов
     */
    fun generateDictionary(n1: Int, n2: Int): Dictionary

    // region Теория
    /**
     * Теоритическое значение длины программы по неравенству Иенсена
     *
     * @param n1 Кол-во операторов
     * @param n2 Кол-во операндов
     */
    fun programSize(n1: Int, n2: Int): Double

    /**
     * Теоритическое значение длины программы согласно соотношению Холстеда
     *
     * @param size Размер словаря
     */
    fun programSize(size: Int): Double

    /**
     * Теоритическое значение дисперсии длины программы
     *
     * @param size Размер словаря
     */
    fun variance(size: Int): Double


    /**
     * Теоритическое стандартное отклонение длины программы
     *
     * @param size Размер словаря
     */
    fun sigma(size: Int): Double

    /**
     * Теоритическое значение относительной ожидаемой погрешности длины программы
     *
     * @param size Размер словаря
     */
    fun delta(size: Int): Double
    // endregion Теория

    // region Практика
    /**
     * Значение длины программы
     */
    fun programSize(dictionary: Dictionary): Long

    /**
     * Среднее значение длины программы
     */
    fun medianProgramSize(data: List<Dictionary>): Long

    /**
     * Математическое ожидание длины программы
     */
    fun mean(data: List<Dictionary>): Double

    /**
     * Дисперсия длины программы
     */
    fun variance(data: List<Dictionary>): Double

    /**
     * Стандартное отклонение длины программы
     */
    fun sigma(data: List<Dictionary>): Double

    /**
     * Относительная ожидаемая погрешность длины программы
     */
    fun delta(data: List<Dictionary>): Double

    // endregion Практика
}
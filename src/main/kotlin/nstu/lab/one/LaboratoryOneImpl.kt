package nstu.lab.one

import Index

/**
 * Реализация [LaboratoryOne]
 */
class LaboratoryOneImpl : LaboratoryOne {
    override fun dotProductNonZeroOrNull(
        indexes: Array<Index>,
        values: Array<Int>
    ): Int? {
        var modified = false
        var product = 1
        indexes
            .filter { it.value in values.indices }
            .forEach {
                if (values[it.value] != 0) {
                    product *= values[it.value]
                    modified = true
                }
            }
        return if (modified) product else null
    }

    override fun findMinOrNull(data: Array<Int>): Pair<Int, Index>? {
        if (data.isEmpty()) return null
        var index = 0
        var min = data[index]
        for (i in 1..data.lastIndex) {
            val e = data[i]
            if (min > e) {
                min = e
                index = i
            }
        }
        return Pair(min, Index(index))
    }

    override fun reverse(data: Array<Float>) {
        val midPoint = (data.size / 2) - 1
        if (midPoint < 0) return
        var rIndex = data.lastIndex
        for (index in 0..midPoint) {
            val tmp = data[index]
            data[index] = data[rIndex]
            data[rIndex] = tmp
            rIndex--
        }
    }
}
package nstu.lab.seven

import kotlin.math.ln
import kotlin.math.log2
import kotlin.math.pow
import kotlin.math.roundToInt

class LaboratorySevenImpl : LaboratorySeven {

    override fun numberLowLevelModules(eta: Int): Int = (eta / 8.0).roundToInt()

    override fun numberHierarchy(eta: Int): Int {
        val k = numberLowLevelModules(eta)
        return if (k > 8) (log2(eta.toDouble()) / 3.0).toInt() + 1 else 1
    }

    override fun numberSharedLevelModules(eta: Int): Int {
        val k = numberLowLevelModules(eta)
        return if (k > 8) getK(eta).roundToInt() else 0
    }

    override fun programSize(eta: Int): Int {
        val K = getK(eta)
        val eta2k = getEta2k(eta)
        val Nk = 2.0 * eta2k * log2(eta2k)

        return (K * Nk).roundToInt()
    }

    override fun programVolume(eta: Int): Int {
        val K = getK(eta)
        val eta2k = getEta2k(eta)
        val Nk = 2f * eta2k * log2(eta2k)

        return (K * Nk * log2(2 * eta2k)).roundToInt()
    }

    override fun programSizeAsm(eta: Int): Int = (3f / 8f * programSize(eta)).roundToInt()

    override fun countTime(eta: Int, nu: Int, n: Int): Int = (3.0 * programSize(eta) / (8.0 * n * nu)).roundToInt()

    override fun numberErrors(eta: Int): Float = programVolume(eta) / 3_000f

    override fun countReliability(eta: Int, tau: Float): Float = tau / ln(numberErrors(eta))

    private fun getK(eta: Int): Double {
        val k = eta / 8.0
        val i = numberHierarchy(eta)
        var K = 1.0
        for (j in 1 until i)
            K += eta / 8.0.pow(j.toDouble())
        return if (k > 8) K else k
    }

    private fun getEta2k(eta: Int): Double {
        val eta2StartK = eta / getK(eta)
        return eta2StartK * log2(eta2StartK)
    }
}
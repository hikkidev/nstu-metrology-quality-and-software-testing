package nstu.lab.seven

interface LaboratorySeven {
    /**
     * @param eta Эта2*
     *
     * @return число модулей k ПС нижнего уровня
     */
    fun numberLowLevelModules(eta: Int): Int

    /**
     * Рассчитать число уровней иерархии i
     *
     * @param eta Эта2*
     */
    fun numberHierarchy(eta: Int): Int

    /**
     * @param eta Эта2*
     *
     * @return общее число K модулей в ПС.
     */
    fun numberSharedLevelModules(eta: Int): Int

    /**
     * @param eta Эта2*
     *
     * @return длину N программы.
     */
    fun programSize(eta: Int): Int

    /**
     * @param eta Эта2*
     *
     * @return объем V ПС.
     */
    fun programVolume(eta: Int): Int

    /**
     * @param eta Эта2*
     *
     * @return длина ПС, выраженную в количестве команд ассемблера P.
     */
    fun programSizeAsm(eta: Int): Int

    /**
     * @param eta Эта2*
     * @param nu Ню
     * @param n Число программистов
     *
     * @return календарное время программирования Tk.
     */
    fun countTime(eta: Int, nu: Int, n: Int): Int

    /**
     * @param eta Эта2*
     * @param nu Ню
     *
     * @return начальное количество ошибок.
     */
    fun numberErrors(eta: Int): Float

    /**
     * @param eta Эта2*
     * @param tau Время отладки
     *
     * @return надежность ПС tн.
     */
    fun countReliability(eta: Int, tau: Float): Float
}
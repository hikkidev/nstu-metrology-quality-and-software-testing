package nstu.lab.fouth

interface Matrix {
    /**
     * Число строк
     */
    val rows: Int

    /**
     * Число столбцов
     */
    val cols: Int

    /**
     * Диапазон строк
     */
    val rowRange: IntRange

    /**
     * Диапазон столбцов
     */
    val colRange: IntRange

    /**
     * Минимальное значение в [Matrix]
     */
    val minValue: Int

    /**
     * Максимальное значение в [Matrix]
     */
    val maxValue: Int

    /**
     * Является ли матрица квадратной?
     */
    val isSquared: Boolean

    /**
     * Получить элемент матрицы
     *
     * @param row Номер строки (начиная с нуля)
     * @param col Номер столбца (начиная с нуля)
     *
     * @return элемент матрицы по адресу [row, col]
     */
    operator fun get(row: Int, col: Int): Int

    /**
     * Записать элемент в матрицу
     *
     * @param row Номер строки (начиная с нуля)
     * @param col Номер столбца (начиная с нуля)
     */
    operator fun set(row: Int, col: Int, value: Int)

    /**
     * Оператор сложения двух матриц
     *
     * @param other Другая матрица
     *
     * @return Новый объект [Matrix] элементы которого,
     * получены путём сложения элементов [this] и [other] с одинаковыми индексами.
     */
    operator fun plus(other: Matrix): Matrix

    /**
     * Оператор вычетания двух матриц
     *
     * @param other Другая матрица
     *
     * @return Новый объект [Matrix] элементы которого,
     * получены путём вычитания элементов [this] и [other] с одинаковыми индексами.
     */
    operator fun minus(other: Matrix): Matrix

    /**
     * Оператор умножения двух матриц
     *
     * @param other Другая матрица
     *
     * @return Новый объект [Matrix] элементы которого, получены по правилам умножения матриц.
     */
    operator fun times(other: Matrix): Matrix

    /**
     * Транспонировать матрицу
     *
     * @return Новый объект [Matrix] элементы которого, получены путём транспонирования элементов [this].
     */
    fun transpose(): Matrix
}

/***
 * Форматировать [Matrix] в строку
 *
 * @sample:
 * {
 *   {1, 0, 0},
 *   {1, 1, 0},
 *   {1, 1, 1}
 *  }
 */
fun Matrix.format(): String {
    val builder = StringBuilder()
    builder.append("{\n {")
    for ((count, row) in rowRange.withIndex()) {
        if (count > 0) {
            builder.append("},\n {")
        }
        for ((innerCount, col) in colRange.withIndex()) {
            if (innerCount > 0) builder.append(", ")
            builder.append(this[row, col].toString())
        }
    }
    builder.append("}\n}")
    return builder.toString()
}

/**
 * Returns true if all elements match the given predicate.
 */
inline fun Matrix.all(crossinline predicate: (Int) -> Boolean): Boolean {
    var flag = false
    forEach { flag = predicate(it) }
    return flag
}

/**
 * Performs the given [action] on each element, providing row, col indexes with the element.
 * @param [action] function that takes the index of an element and the element itself
 * and performs the action on the element.
 */
inline fun Matrix.forEachIndexed(action: (Int, Int) -> Unit) {
    for (row in rowRange)
        for (col in colRange)
            action.invoke(row, col)
}

/**
 * Performs the given [action] on each element.
 */
inline fun Matrix.forEach(action: (Int) -> Unit) {
    for (row in rowRange)
        for (col in colRange)
            action.invoke(this[row, col])
}

/**
 * Set each element the given [action].
 */
inline fun Matrix.onEachIndexed(action: (Int, Int) -> Int) {
    for (row in rowRange)
        for (col in colRange)
            this[row, col] = action.invoke(row, col)
}
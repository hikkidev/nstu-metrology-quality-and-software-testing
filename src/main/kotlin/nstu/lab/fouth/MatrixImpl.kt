package nstu.lab.fouth

/**
 * Класс описывает двумерную матрицу состояющую из целочисленных элементов
 *
 * @property rows Число строк
 * @property cols Число столбцов
 */
class MatrixImpl(
    override val rows: Int,
    override val cols: Int
) : Matrix {
    /**
     * Диапазон строк
     */
    override val rowRange: IntRange = 0 until rows

    /**
     * Диапазон столбцов
     */
    override val colRange: IntRange = 0 until cols

    /**
     * Поиск минимального значения
     */
    override val minValue: Int
        get() {
            var min = Int.MAX_VALUE
            forEach { element ->
                if (element < min) min = element
            }
            return min
        }

    /**
     * Поиск максимального значения
     */
    override val maxValue: Int
        get() {
            var max = Int.MIN_VALUE
            forEach { element ->
                if (element > max) max = element
            }
            return max
        }

    /**
     * Является ли матрица квадратной?
     */
    override val isSquared: Boolean = rows == cols

    private val values: IntArray

    init {
        require(rows > 0)
        require(cols > 0)
        values = IntArray(rows * cols)
    }

    /**
     * Получить элемент матрицы
     *
     * @param row Номер строки (начиная с нуля)
     * @param col Номер столбца (начиная с нуля)
     *
     * @return элемент матрицы по адресу [row, col]
     */
    override operator fun get(row: Int, col: Int): Int {
        require(row in rowRange)
        require(col in colRange)
        return values[row * cols + col]
    }

    /**
     * Записать элемент в матрицу
     *
     * @param row Номер строки (начиная с нуля)
     * @param col Номер столбца (начиная с нуля)
     */
    override operator fun set(row: Int, col: Int, value: Int) {
        require(row in rowRange)
        require(col in colRange)
        values[row * cols + col] = value
    }

    /**
     * Оператор сложения двух матриц
     *
     * @param other Другая матрица
     *
     * @return Новый объект [Matrix] элементы которого,
     * получены путём сложения элементов [this] и [other] с одинаковыми индексами.
     */
    override operator fun plus(other: Matrix): Matrix {
        require(rows == other.rows)
        require(cols == other.cols)
        return MatrixImpl(rows, cols).also { out ->
            out.forEachIndexed { row, col ->
                out[row, col] = this[row, col] + other[row, col]
            }
        }
    }

    /**
     * Оператор вычетания двух матриц
     *
     * @param other Другая матрица
     *
     * @return Новый объект [Matrix] элементы которого,
     * получены путём вычитания элементов [this] и [other] с одинаковыми индексами.
     */
    override operator fun minus(other: Matrix): Matrix {
        require(rows == other.rows)
        require(cols == other.cols)
        return MatrixImpl(rows, cols).also { out ->
            out.forEachIndexed { row, col ->
                out[row, col] = this[row, col] - other[row, col]
            }
        }
    }

    /**
     * Оператор умножения двух матриц
     *
     * @param other Другая матрица
     *
     * @return Новый объект [Matrix] элементы которого, получены по правилам умножения матриц.
     */
    override operator fun times(other: Matrix): Matrix {
        require(rows == other.rows)
        require(isSquared && other.isSquared)
        return MatrixImpl(rows, cols).also { out ->
            forEachIndexed { row, col ->
                out[row, col] = colRange.fold(0) { acc, innerCol ->
                    this[row, innerCol] * other[innerCol, col] + acc
                }
            }
        }
    }

    /**
     * Транспонировать матрицу
     *
     * @return Новый объект [Matrix] элементы которого, получены путём транспонирования элементов [this].
     */
    override fun transpose(): Matrix {
        check(isSquared)
        return MatrixImpl(rows, cols).also { out ->
            out.forEachIndexed { row, col ->
                out[row, col] = this[col, row]
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        return other is Matrix && rows == other.rows && cols == other.cols && contentEquals(other)
    }

    override fun toString(): String {
        val separator = ", "
        val builder = StringBuilder()

        builder.append("{{")
        for ((count, row) in rowRange.withIndex()) {
            if (count > 0) {
                builder.append('}')
                builder.append(separator)
                builder.append('{')
            }
            for ((innerCount, col) in colRange.withIndex()) {
                if (innerCount > 0) builder.append(separator)
                builder.append(this[row, col].toString())
            }
        }
        builder.append("}}")
        return builder.toString()
    }

    override fun hashCode(): Int {
        return values.contentHashCode()
    }

    private fun contentEquals(other: Matrix): Boolean {
        if (this === other) return true
        if (rows != other.rows || cols != other.cols) return false
        forEachIndexed { row, col -> if (this[row, col] != other[row, col]) return false }
        return true
    }
}
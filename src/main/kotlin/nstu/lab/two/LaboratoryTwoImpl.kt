package nstu.lab.two

import nstu.lab.three.Matrix

class LaboratoryTwoImpl : LaboratoryTwo {

    override fun <T> min(a: T, b: T, c: T): T where T : Number, T : Comparable<T> {
        return minOf(a, minOf(b, c))
    }

    override fun findSum(array: TwoDimensionArray): Double {
        return array.indices
            .map { it to array[it].indices }
            .flatMap { (row, cols) ->
                cols
                    .filter { col -> (row + col) % 2 == 0 }
                    .map { col -> row to col }
            }
            .fold(0.0) { acc, (row, col) -> acc + array[row][col] }
    }

    override fun findMaxBelowDiagonal(matrix: Matrix): Double? {
        return matrix.rowsIndices
            .mapNotNull { index ->
                (0..minOf(index, matrix.maxCols - 1))
                    .maxOfOrNull { matrix[index][it] }
            }
            .maxOrNull()
    }
}
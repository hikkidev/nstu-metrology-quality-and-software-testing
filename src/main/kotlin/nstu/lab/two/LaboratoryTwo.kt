package nstu.lab.two

import nstu.lab.three.Matrix

/**
 * Постройте УГП и разработайте тестовые наборы данных для тестирования функций разработанных по критерию С1.
 */
interface LaboratoryTwo {

    /**
     * Поиск минимума из трёх чисел.
     */
    fun <T> min(a: T, b: T, c: T): T where T : Number, T : Comparable<T>

    /**
     * Отыскивает  и  возвращает  сумму  значений  компонентов  массива,
     * у которых сумма значений индексов – чётная.
     *
     * @param array Двумерный  массив  вещественных  переменных
     */
    fun findSum(array: TwoDimensionArray): Double

    /**
     * Отыскивает и возвращает максимальное значение компонентов массива, лежащих на и ниже главной диагонали или null,
     * если [matrix] пуст или вложенные массивы пусты.
     *
     * @param matrix Двумерный  массив  вещественных  переменных
     */
    fun findMaxBelowDiagonal(matrix: Matrix): Double?
}
package nstu.lab.six

import Index


typealias TwoDimensionArray = Array<IntArray>

/**
 * Вспомогательная функция перестановки двух элементов в массиве
 *
 * @param [this] Одномерный массив целых чисел.
 * @param i Индекс
 * @param j Индекс
 *
 * @return [this]
 */
inline fun IntArray.swap(i: Int, j: Int) {
    val buffer = this[i]
    this[i] = this[j]
    this[j] = buffer
}

/**
 * Функция получает одномерный массив целых переменных. Вычисляет и возвращает минимальный по значению
 * элемент этого массива и номер его индекса.
 *
 * @param [this] Одномерный массив целых чисел.
 *
 * @return Пара значений: минимальный по значению элемент [this] и его индекс или null.
 */
fun IntArray.findMinOrNull(): Pair<Int, Index>? {
    if (isEmpty()) return null
    var index = 0
    var min = this[index]
    for (i in 1..lastIndex) {
        val e = this[i]
        if (min > e) {
            min = e
            index = i
        }
    }
    return Pair(min, Index(index))
}

/**
 * Сортирует текущий массив в порядке возрастания методом пузырька.
 *
 * @param [this] Одномерный массив целых чисел.
 *
 * @return [this]
 */
fun IntArray.bubbleSortAsc(): IntArray {
    var sorted: Boolean
    do {
        sorted = true
        for (i in 1 until size) if (this[i - 1] > this[i]) {
            swap(i - 1, i)
            sorted = false
        }
    } while (!sorted)
    return this
}

/**
 * Функция осуществляет бинарный поиск элемента в упорядоченном одномерном массиве.
 *
 * @param [this] Упорядоченный одномерный массив целых чисел.
 * @param [element] Искомый элемент
 *
 * @return Индекс [element] в массиве [this] или -1, если элемент не найден.
 */
fun IntArray.binarySearch(element: Int): Int {
    var firstIndex = 0
    var lastIndex = lastIndex
    if (lastIndex == -1) return -1

    while (firstIndex <= lastIndex) {
        val avgIndex = (lastIndex + firstIndex) / 2
        when {
            element > this[avgIndex] -> firstIndex = avgIndex + 1
            element < this[avgIndex] -> lastIndex = avgIndex - 1
            else -> return avgIndex
        }
    }
    return -1
}

/**
 * Отыскать минимальный элемент двумерного массива целых чисел.
 *
 * @param [this] Двумерный массив целых чисел.
 *
 * @return минимальный по значению элемент этого массива и номера индексов или null.
 */
fun TwoDimensionArray.findMinOrNull(): Triple<Int, Index, Index>? {
    return this
        .mapIndexedNotNull { index, data -> data.findMinOrNull()?.let { Triple(it.first, Index(index), it.second) } }
        .minByOrNull { it.first }
}

/**
 * Осуществить перестановку значений элементов текущего массива в обратном порядке.
 *
 * @param [this] Одномерный массив целых чисел.
 *
 * @return [this]
 */
fun IntArray.reverseOrder(): IntArray {
    val avgIndex = (size / 2) - 1
    if (avgIndex < 0) return this

    var reverseIndex = lastIndex
    for (index in 0..avgIndex) {
        swap(index, reverseIndex)
        reverseIndex--
    }
    return this
}

/**
 * Осуществлять циклический сдвиг элементов одномерного массива на заданное число позиций влево.
 *
 * @param [this] Одномерный массив целых чисел.
 * @param shift Сдвиг.
 *
 * @return Новый массив.
 */
fun IntArray.rotateLeftBy(shift: Int): IntArray {
    var shift = shift % size
    if (shift < 0) shift += size

    for (i in 0 until shift) {
        val buffer = this[0]
        for (j in 0 until lastIndex)
            this[j] = this[j + 1]
        this[lastIndex] = buffer
    }
    return this
}

/**
 * Заменяет все вхождения целочисленного значения в текущем массиве.
 *
 * @param [this] Одномерный массив целых чисел.
 *
 * @param oldValue Искомое значение.
 * @param newValue Новое значение.
 *
 * @return [this]
 */
fun IntArray.replaceAll(oldValue: Int, newValue: Int): IntArray {
    forEachIndexed { index, element ->
        if (element == oldValue) this[index] = newValue
    }
    return this
}

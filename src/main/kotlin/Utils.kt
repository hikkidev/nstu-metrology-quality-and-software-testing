import kotlin.math.abs
import kotlin.math.log10
import kotlin.math.pow

fun Long.countDigits(): Int = maxOf(0, log10(abs(this.toDouble())).toInt()) + 1
fun Int.countDigits(): Int = this.toLong().countDigits()

inline fun Double.takeIf(predicate: (Double) -> Boolean) = if (predicate(this)) this else 0.0
inline fun Int.pow(pow: Double) = this.toDouble().pow(pow).toLong()
inline fun Int.pow(pow: Int) = this.toDouble().pow(pow).toLong()

val Double.isEven: Boolean get() = this % 2 == 0.0
val Int.isEven: Boolean get() = this % 2 == 0
val Int.isOdd: Boolean get() = !this.isEven


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import nstu.lab.five.Dictionary
import nstu.lab.five.LaboratoryFive
import nstu.lab.five.LaboratoryFiveImpl
import nstu.lab.seven.LaboratorySeven
import nstu.lab.seven.LaboratorySevenImpl
import nstu.lab.six.reverseOrder
import java.util.concurrent.Executors

val exec = Executors.newSingleThreadExecutor().asCoroutineDispatcher()
val scope = CoroutineScope(exec)


fun main(args: Array<String>) {
    runLabSeven()
}

fun runLabFive() {
    val dictionarySize = 128
    val n1 = (dictionarySize * 0.5).toInt()
    val n2 = dictionarySize - n1

    println("Размер словаря: $dictionarySize\n Число операторов: $n1\n Число операндов: $n2")
    val target: LaboratoryFive = LaboratoryFiveImpl()

    val times = 1000
    println(" Число экспериментов $times\n")

    val dicts = mutableListOf<Dictionary>()
    do {
        dicts.clear()

        repeat(times) {
            val dictionary = target.generateDictionary(n1, n2)
            dicts.add(dictionary)
        }
    } while (target.medianProgramSize(dicts) < 750)

    println("Длина программы согласно соотношению Холстеда: ${target.programSize(dictionarySize)}")
    println("Практическая длина программы Q               : ${target.medianProgramSize(dicts)}")

    println("\nТеоритическая дисперсия: ${target.variance(dictionarySize)}")
    println("Практическая дисперсия : ${target.variance(dicts)}")

    println("\nТеоритическая сигма: ${target.sigma(dictionarySize)}")
    println("Практическая сигма : ${target.sigma(dicts)}")

    println("\nТеоритическая дельта: ${target.delta(dictionarySize)}")
    println("Практическая дельта : ${target.delta(dicts)}")
}

fun runLabSix() {
    val data = intArrayOf(1, 2, 3, 4)
    data.reverseOrder().also { println(it.joinToString()) }
//    val data = intArrayOf(5, 4, 3, 2, -8, 9, 0).also { println(it.joinToString()); it.reverseOrder() }
//    println(data.joinToString())

//    data
//        .sortedArray()
//        .also { println(it.joinToString()) }
//        .binarySearch( 1)
//        .also { println("Index: $it") }

//    val values = arrayOf(
//        intArrayOf(0, 5, 1),
//        intArrayOf(0, -5, 1),
//        intArrayOf(),
//        intArrayOf(-11, 50, 7),
//    )
//    values.findMinOrNull()
}

fun runLabSeven() {
    val n = 5
    val nu = 20
    val target: LaboratorySeven = LaboratorySevenImpl()

    val etas = listOf(300, 400, 512, 8, 12)
    for (eta in etas) {
        println("eta2*: $eta")
        println("k: ${target.numberLowLevelModules(eta)}")
        println("i: ${target.numberHierarchy(eta)}")
        println("K: ${target.numberSharedLevelModules(eta)}")
        println("N: ${target.programSize(eta)}")
        println("V: ${target.programVolume(eta)}")
        println("P: ${target.programSizeAsm(eta)}")
        println("Tk: ${target.countTime(eta, nu, n)}")
        println("B0: ${target.numberErrors(eta)}")
        val tau = 0.5f * target.countTime(eta, nu, n)
        println("tH: ${target.countReliability(eta, tau)}")
        println("")
    }
}
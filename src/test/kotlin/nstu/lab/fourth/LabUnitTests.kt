import nstu.lab.fouth.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@DisplayName("Лабораторная работа №4")
class LaboratoryFourthUnitTests {

    @Nested
    @DisplayName("Тестирование конструктора")
    inner class LaboratoryFourthConstructor {

        @Test
        @DisplayName("Создание матрицы 3х3")
        fun `3x3 constructor`() {
            val size = 3
            val matrix: Matrix = MatrixImpl(size, size)

            assertTrue { matrix.rows == size && matrix.cols == size }
        }

        @Test
        @DisplayName("Создание матрицы 2х3")
        fun `2x3 constructor`() {
            val rows = 2
            val cols = 3
            val matrix: Matrix = MatrixImpl(rows, cols)

            assertTrue { matrix.rows == rows && matrix.cols == cols }
        }

        @Test
        @DisplayName("Создание матрицы 3х2")
        fun `3x2 constructor`() {
            val rows = 3
            val cols = 2
            val matrix: Matrix = MatrixImpl(rows, cols)

            assertTrue { matrix.rows == rows && matrix.cols == cols }
        }

        @Test
        @DisplayName("Ошибка создания объекта при числе строк равных 0")
        fun `rows eq zero constructor`() {
            val error = kotlin.runCatching { MatrixImpl(0, 2) }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }

        @Test
        @DisplayName("Ошибка создания объекта при числе столбцов равных 0")
        fun `cols eq zero constructor`() {
            val error = kotlin.runCatching { MatrixImpl(3, 0) }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }
    }

    @Nested
    @DisplayName("Тестирование операторов set/get")
    inner class LaboratoryFourthSet {

        @Test
        @DisplayName("Доступ на запись и чтение каждого элемента матрицы")
        fun `each element`() {
            val size = 3
            val matrix: Matrix = MatrixImpl(size, size)
            matrix.forEachIndexed { row, col -> matrix[row, col] = 1 }
            assertTrue { matrix.all { it == 1 } }
        }

        @Test
        @DisplayName("Ошибка записи в элемент за пределами столбца")
        fun `wrong write col`() {
            val matrix: Matrix = MatrixImpl(3, 3)

            val error = kotlin.runCatching { matrix[0, 3] = 1 }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }

        @Test
        @DisplayName("Ошибка записи в элемент за пределами строки")
        fun `wrong write row`() {
            val matrix: Matrix = MatrixImpl(3, 3)

            val error = kotlin.runCatching { matrix[3, 0] = 1 }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }

        @Test
        @DisplayName("Ошибка записи в элемент за пределами строки и столбца")
        fun `wrong write row and cols`() {
            val matrix: Matrix = MatrixImpl(3, 3)

            val error = kotlin.runCatching { matrix[3, 3] = 1 }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }

        @Test
        @DisplayName("Ошибка чтения элемента за пределами столбца")
        fun `wrong read col`() {
            val matrix: Matrix = MatrixImpl(3, 3)

            val error = kotlin.runCatching { matrix[0, 3] }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }

        @Test
        @DisplayName("Ошибка чтения элемента за пределами строки")
        fun `wrong read row`() {
            val matrix: Matrix = MatrixImpl(3, 3)

            val error = kotlin.runCatching { matrix[3, 0] }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }

        @Test
        @DisplayName("Ошибка чтения элемента за пределами строки и столбца")
        fun `wrong read row and cols`() {
            val matrix: Matrix = MatrixImpl(3, 3)

            val error = kotlin.runCatching { matrix[3, 3] }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }
    }

    @Nested
    @DisplayName("Тестирование оператора +")
    inner class LaboratoryFourthPlus {

        @Test
        @DisplayName("Сложение матрицы 2х2")
        fun `2x2 matrix`() {
            val a: Matrix = MatrixImpl(2, 2)
            val b: Matrix = MatrixImpl(2, 2)
            val c: Matrix = MatrixImpl(2, 2)

            a[0, 0] = -2; a[0, 1] = 2; a[1, 0] = 1
            b[0, 0] = 1; b[1, 1] = 1;
            c[0, 0] = -1; c[0, 1] = 2; c[1, 0] = 1; c[1, 1] = 1
            assertEquals(c, a + b)
        }

        @Test
        @DisplayName("Ошибка число строк не совпадает")
        fun `wrong rows`() {
            val a: Matrix = MatrixImpl(3, 3)
            val b: Matrix = MatrixImpl(2, 3)

            val error = kotlin.runCatching { a + b }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }

        @Test
        @DisplayName("Ошибка число столбцов не совпадает")
        fun `wrong cols`() {
            val a: Matrix = MatrixImpl(3, 3)
            val b: Matrix = MatrixImpl(3, 2)

            val error = kotlin.runCatching { a + b }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }
    }

    @Nested
    @DisplayName("Тестирование оператора -")
    inner class LaboratoryFourthMinus {

        @Test
        @DisplayName("Сложение квадратных матриц")
        fun `2x2 matrix`() {
            val a: Matrix = MatrixImpl(2, 2)
            val b: Matrix = MatrixImpl(2, 2)
            val c: Matrix = MatrixImpl(2, 2)

            a[0, 0] = -2; a[0, 1] = 2; a[1, 0] = 1
            b[0, 0] = 1; b[1, 1] = 1;
            c[0, 0] = -3; c[0, 1] = 2; c[1, 0] = 1; c[1, 1] = -1
            assertEquals(c, a - b)
        }

        @Test
        @DisplayName("Ошибка число строк не совпадает")
        fun `wrong rows`() {
            val a: Matrix = MatrixImpl(3, 3)
            val b: Matrix = MatrixImpl(2, 3)

            val error = kotlin.runCatching { a - b }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }

        @Test
        @DisplayName("Ошибка число столбцов не совпадает")
        fun `wrong cols`() {
            val a: Matrix = MatrixImpl(3, 3)
            val b: Matrix = MatrixImpl(3, 2)

            val error = kotlin.runCatching { a - b }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }
    }

    @Nested
    @DisplayName("Тестирование оператора *")
    inner class LaboratoryFourthTimes {

        @Test
        @DisplayName("Умножение матриц 3x3")
        fun `3x3 matrix`() {
            val size = 3
            val a: Matrix = MatrixImpl(size, size)
            val b: Matrix = MatrixImpl(size, size)
            val c: Matrix = MatrixImpl(size, size)
            a.forEachIndexed { row, col -> if (row <= col) a[row, col] = 1 }
            b.forEachIndexed { row, col -> if (row >= col) b[row, col] = 1 }
            c[0, 0] = 3;c[0, 1] = 2; c[1, 0] = 2;c[1, 1] = 2;
            c[0, 2] = 1;c[1, 2] = 1;c[2, 0] = 1; c[2, 1] = 1; c[2, 2] = 1
            assertEquals(c, a * b)
        }

        @Test
        @DisplayName("Ошибка число строк не совпадает")
        fun `wrong rows`() {
            val a: Matrix = MatrixImpl(3, 3)
            val b: Matrix = MatrixImpl(2, 3)

            val error = kotlin.runCatching { a * b }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }

        @Test
        @DisplayName("Ошибка число столбцов не совпадает")
        fun `wrong cols`() {
            val a: Matrix = MatrixImpl(3, 3)
            val b: Matrix = MatrixImpl(3, 2)

            val error = kotlin.runCatching { a * b }.exceptionOrNull()
            assertTrue { error is IllegalArgumentException }
        }
    }

    @Nested
    @DisplayName("Тестирование оператора transpose")
    inner class LaboratoryFourthTranspose {

        @Test
        @DisplayName("Транспонирование матрицы 3x3")
        fun `3x3 matrix`() {
            val size = 3
            val a: Matrix = MatrixImpl(size, size)
            val b: Matrix = MatrixImpl(size, size)
            var index = 1
            a.onEachIndexed { _, _ -> index++ }.also { index = 1 }

            for (row in b.rowRange)
                for (col in b.colRange)
                    b[col, row] = index++

            assertEquals(b, a.transpose())
        }

        @Test
        @DisplayName("Ошибка число строк не совпадает")
        fun `wrong rows`() {
            val b: Matrix = MatrixImpl(2, 3)

            val error = kotlin.runCatching { b.transpose() }.exceptionOrNull()
            assertTrue { error is IllegalStateException }
        }

        @Test
        @DisplayName("Ошибка число столбцов не совпадает")
        fun `wrong cols`() {
            val b: Matrix = MatrixImpl(3, 2)

            val error = kotlin.runCatching { b.transpose() }.exceptionOrNull()
            assertTrue { error is IllegalStateException }
        }
    }

    @Nested
    @DisplayName("Тестирование оператора equals")
    inner class LaboratoryFourthEquals {

        @Test
        @DisplayName("Матрицы совпадают")
        fun `3x3 matrix`() {
            val size = 3
            val a: Matrix = MatrixImpl(size, size)
            val b: Matrix = MatrixImpl(size, size)
            a.onEachIndexed { _, _ -> 1 }
            b.onEachIndexed { _, _ -> 1 }

            assertTrue { a == b }
        }

        @Test
        @DisplayName("Контент не совпадает")
        fun `wrong content matrix`() {
            val size = 3
            val a: Matrix = MatrixImpl(size, size)
            val b: Matrix = MatrixImpl(size, size)
            a.onEachIndexed { _, _ -> 1 }
            b.onEachIndexed { _, _ -> 1 }
            b[0, 0] = 0
            assertTrue { a != b }
        }

        @Test
        @DisplayName("Ошибка число строк не совпадает")
        fun `wrong rows`() {
            val a: Matrix = MatrixImpl(3, 3)
            val b: Matrix = MatrixImpl(2, 3)

            assertTrue { a != b }
        }

        @Test
        @DisplayName("Ошибка число столбцов не совпадает")
        fun `wrong cols`() {
            val a: Matrix = MatrixImpl(3, 3)
            val b: Matrix = MatrixImpl(3, 2)

            assertTrue { a != b }
        }
    }

    @Nested
    @DisplayName("Тестирование оператора флага isSquared")
    inner class LaboratoryFourthSquared {

        @Test
        @DisplayName("Квадратная матрица")
        fun `3x3 matrix`() {
            val size = 3
            val a: Matrix = MatrixImpl(size, size)
            assertTrue { a.isSquared }
        }

        @Test
        @DisplayName("Ошибка число строк не совпадает")
        fun `wrong rows`() {
            val b: Matrix = MatrixImpl(2, 3)

            assertTrue { !b.isSquared }
        }

        @Test
        @DisplayName("Ошибка число столбцов не совпадает")
        fun `wrong cols`() {
            val b: Matrix = MatrixImpl(3, 2)

            assertTrue { !b.isSquared }
        }
    }

    @Test
    @DisplayName("Форматирование в строку")
    fun `formatMatrix`() {
        val size = 3
        val a: Matrix = MatrixImpl(size, size)
        var index = 1
        a.onEachIndexed { _, _ -> index++ }

        val expected = "{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}"
        assertEquals(expected, a.toString())
    }

    @Test
    @DisplayName("Минимальное значение")
    fun `minValue`() {
        val size = 3
        val a: Matrix = MatrixImpl(size, size)
        var index = 1
        a.onEachIndexed { _, _ -> index++ }

        assertEquals(1, a.minValue)
    }

    @Test
    @DisplayName("Максимальное значение")
    fun `maxValue`() {
        val size = 3
        val a: Matrix = MatrixImpl(size, size)
        var index = 1
        a.onEachIndexed { _, _ -> index++ }

        assertEquals(9, a.maxValue)
    }
}
import nstu.lab.three.matrixOf
import nstu.lab.two.LaboratoryTwo
import nstu.lab.two.LaboratoryTwoImpl
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import kotlin.test.Test
import kotlin.test.assertEquals

@DisplayName("Лабораторная работа №2")
class LaboratoryTwoUnitTests {
    private val targetClass: LaboratoryTwo = LaboratoryTwoImpl()

    private fun fibonacci(): Sequence<Double> {
        return generateSequence(0.0 to 1.0) { it.second to it.first + it.second }.map { it.first }
    }

    @Nested
    @DisplayName("Тестирование функции min")
    inner class LaboratoryTwoMin {

        @Test
        @DisplayName("Поиск минимума на множестве Integer")
        fun `min function over Integer`() {
            val expected = Int.MIN_VALUE
            val actual = targetClass.min(0, Int.MIN_VALUE, Int.MAX_VALUE)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Поиск минимума на множестве Long")
        fun `min function over Long`() {
            val expected = Long.MIN_VALUE
            val actual = targetClass.min(0L, Long.MIN_VALUE, Int.MIN_VALUE.toLong())

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Поиск минимума на множестве Float")
        fun `min function over Float`() {
            val expected = Float.MIN_VALUE
            val actual = targetClass.min(1f, Float.MIN_VALUE, Float.MAX_VALUE)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Поиск минимума на множестве Double")
        fun `min function over Double`() {
            val expected = Double.MIN_VALUE
            val actual = targetClass.min(1.0, Float.MIN_VALUE.toDouble(), Double.MIN_VALUE)

            assertEquals(expected, actual)
        }
    }

    @Nested
    @DisplayName("Тестирование функции findSum")
    inner class LaboratoryTwoFindSum {

        @Test
        @DisplayName("Сумма равна нулю на пустой массив")
        fun `returns zero on empty array`() {
            val values = arrayOf<DoubleArray>()
            val expected = 0.0
            val actual = targetClass.findSum(values)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Сумма равна нулю на пустой вложенный массив")
        fun `returns zero on empty nested array`() {
            val values = arrayOf(doubleArrayOf())
            val expected = 0.0
            val actual = targetClass.findSum(values)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Проверка всех ветвей")
        fun `check all branches by criteria C1`() {

            val values = arrayOf(
                doubleArrayOf(0.0, Double.NaN, 1.0),
                doubleArrayOf(Double.NaN, 1.0, Double.NaN, 2.0),
                doubleArrayOf(3.0, Double.NaN, 5.0),
                doubleArrayOf(Double.MAX_VALUE)
            )
            val expected = fibonacci().take(6).sum()
            val actual = targetClass.findSum(values)

            assertEquals(expected, actual)
        }
    }

    @Nested
    @DisplayName("Тестирование функции findMaxBelowDiagonal")
    inner class LaboratoryTwoFindMaxBelowDiagonal {
        @Test
        @DisplayName("Возвращает null на пустой массив")
        fun `returns null on empty array`() {
            val values = matrixOf()
            val expected: Double? = null
            val actual = targetClass.findMaxBelowDiagonal(values)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Возвращает null на пустой вложенный массив")
        fun `returns null on empty nested array`() {
            val values = matrixOf(
                doubleArrayOf()
            )
            val expected: Double? = null
            val actual = targetClass.findMaxBelowDiagonal(values)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Проверка всех ветвей")
        fun `check all branches by criteria C1`() {
            val values = matrixOf(
                doubleArrayOf(0.0, Double.MAX_VALUE, Double.NaN),
                doubleArrayOf(3.0, 1.0, Double.MAX_VALUE),
                doubleArrayOf(6.0, 4.0, 2.0),
                doubleArrayOf(9.0, 7.0, 5.0),
                doubleArrayOf(10.0, 100.0, 8.0),
            )
            val expected = 100.0
            val actual = targetClass.findMaxBelowDiagonal(values)

            assertEquals(expected, actual)
        }
    }
}
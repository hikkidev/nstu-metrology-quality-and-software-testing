import nstu.lab.three.LaboratoryThree
import nstu.lab.three.LaboratoryThreeImpl
import nstu.lab.three.matrixOf
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import kotlin.test.Test
import kotlin.test.assertEquals

@DisplayName("Лабораторная работа №3")
class LaboratoryThreeUnitTests {
    private val targetClass: LaboratoryThree = LaboratoryThreeImpl()
    private val NaN = Double.NaN

    @Nested
    @DisplayName("Тестирование функции reverseOrderOfOddDigits")
    inner class LaboratoryThreeReverseOrderOfOddDigits {

        @Test
        @DisplayName("Возвращает 0 на 0")
        fun `returns zero on zero`() {
            val expected = 0L
            val actual = targetClass.reverseOrderOfOddDigits(0)
            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Возвращает 57 на 7456")
        fun `check criteria c2`() {
            val expected = 57L
            val actual = targetClass.reverseOrderOfOddDigits(7456)
            assertEquals(expected, actual)
        }
    }

    @Nested
    @DisplayName("Тестирование функции findEvenPosOfEvenDigit ")
    inner class LaboratoryThreeFindEvenPosOfEvenDigit {

        @Test
        @DisplayName("Возвращает 0 позицию для числа 0")
        fun `returns zero on zero`() {
            val expected = 0
            val actual = targetClass.findEvenPosOfEvenDigit(0)
            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Возвращает позицию 2 для числа 4861")
        fun `check criteria c2`() {
            val expected = 2
            val actual = targetClass.findEvenPosOfEvenDigit(4861)
            assertEquals(expected, actual)
        }
    }

    @Nested
    @DisplayName("Тестирование функции rotateRightBy")
    inner class LaboratoryThreeRotateRightBy {
        @Test
        @DisplayName("Обработка отрицательного сдвига на положительном числе")
        fun `negative shift on positive number`() {
            val expected = 456123L
            val actual = targetClass.rotateRightBy(-3, 123456)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Обработка положительного сдвига на положительном числе")
        fun `positive shift on positive number`() {
            val expected = 456123L
            val actual = targetClass.rotateRightBy(3, 123456)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Обработка отрицательного сдвига на отрицательном числе")
        fun `negative shift on negative number`() {
            val expected = -612345L
            val actual = targetClass.rotateRightBy(-5, -123456)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Обработка положительного сдвига на отрицательном числе")
        fun `positive shift on negative number`() {
            val expected = -561234L
            val actual = targetClass.rotateRightBy(2, -123456)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Обработка сдвига равного кол-ву цифр в числе")
        fun `if shift equals count digits`() {
            val expected = 1234L
            val actual = targetClass.rotateRightBy(4, 1234)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Обработка переполнения")
        fun `check overflow`() {
            val expected = 7214748364L
            val actual = targetClass.rotateRightBy(1, Int.MAX_VALUE)

            assertEquals(expected, actual)
        }
    }

    @Nested
    @DisplayName("Тестирование функции findSumAboveSubDiagonal")
    inner class LaboratoryThreeFindSumAboveSubDiagonal {

        @Test
        @DisplayName("Возвращает 0 на пустую матрицу")
        fun `returns zero on empty matrix`() {
            val values = matrixOf()
            val expected = 0.0
            val actual = targetClass.findSumAboveSubDiagonal(values)
            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Возвращает 0 на матрицу с пустыми стоблцами")
        fun `returns zero on matrix with empty cols`() {
            val values = matrixOf(
                doubleArrayOf(),
                doubleArrayOf(),
                doubleArrayOf(),
            )
            val expected = 0.0
            val actual = targetClass.findSumAboveSubDiagonal(values)
            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Возвращает 0 на вектор")
        fun `returns zero on row-vector`() {
            val values = matrixOf(
                doubleArrayOf(2.0, 2.0, 2.0, 1.0)
            )
            val expected = 0.0
            val actual = targetClass.findSumAboveSubDiagonal(values)
            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Возвращает 0 на вектор-столбец")
        fun `returns zero on col-vector`() {
            val values = matrixOf(
                doubleArrayOf(2.0),
                doubleArrayOf(1.0),
                doubleArrayOf(2.0),
            )
            val expected = 0.0
            val actual = targetClass.findSumAboveSubDiagonal(values)
            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Столбцов больше чем строк")
        fun `check cols more than rows`() {
            val values = matrixOf(
                doubleArrayOf(2.0, 2.0, 2.0, NaN),
                doubleArrayOf(1.0, 1.0, NaN, NaN)
            )
            val expected = 6.0
            val actual = targetClass.findSumAboveSubDiagonal(values)
            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Строк больше чем столбцов")
        fun `check rows more than cols`() {
            val values = matrixOf(
                doubleArrayOf(1.0, 2.0, 1.0, NaN),
                doubleArrayOf(2.0, 1.0, NaN, NaN),
                doubleArrayOf(1.0, NaN, NaN, NaN),
                doubleArrayOf(NaN, NaN, NaN, NaN),
                doubleArrayOf(NaN, NaN, NaN, NaN),
            )
            val expected = 4.0
            val actual = targetClass.findSumAboveSubDiagonal(values)
            assertEquals(expected, actual)
        }
    }
}
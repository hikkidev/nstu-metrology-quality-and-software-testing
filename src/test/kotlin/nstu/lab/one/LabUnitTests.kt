import nstu.lab.one.LaboratoryOne
import nstu.lab.one.LaboratoryOneImpl
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.TestFactory
import kotlin.test.Test
import kotlin.test.assertEquals

@DisplayName("Лабораторная работа №1")
class LaboratoryOneUnitTests {
    private val targetClass: LaboratoryOne = LaboratoryOneImpl()

    @Nested
    @DisplayName("Тестирование функции dotProductNonZero")
    inner class LaboratoryOneDotProductNonZero {

        @Test
        @DisplayName("Возвращает null на пустые массивы indexes и values")
        fun `returns null on empty arrays`() {
            val indexes: Array<Index> = emptyArray()
            val values: Array<Int> = emptyArray()

            val expected: Int? = null
            val actual = targetClass.dotProductNonZeroOrNull(indexes, values)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Возвращает null если indexes пустой массив")
        fun `returns null on empty indexes array`() {
            val indexes: Array<Index> = emptyArray()
            val values: Array<Int> = arrayOf(1, 2, 3)

            val expected: Int? = null
            val actual = targetClass.dotProductNonZeroOrNull(indexes, values)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Возвращает null если values пустой массив")
        fun `returns null on empty values array`() {
            val indexes: Array<Index> = Index.arrayOf(1, 2, 3)
            val values: Array<Int> = emptyArray()

            val expected: Int? = null
            val actual = targetClass.dotProductNonZeroOrNull(indexes, values)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Возвращает null если все значения values нулевые")
        fun `check if each value eq 0`() {
            val values: Array<Int> = arrayOf(0, 0, 0, 0)
            val indexes: Array<Index> = Index.arrayOf(values.indices)

            val expected: Int? = null
            val actual = targetClass.dotProductNonZeroOrNull(indexes, values)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Если значения indexes выходят за границы массива values")
        fun `check for Index Bounds`() {
            val indexes: Array<Index> = Index.arrayOf(0, 3, -5, 8)
            val values: Array<Int> = arrayOf(5)

            val expected: Int = values[0]
            val actual = targetClass.dotProductNonZeroOrNull(indexes, values)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Проверка на обычное поведение")
        fun `check default behaviour`() {
            val indexes: Array<Index> = Index.arrayOf(-5..5)
            val values: Array<Int> = arrayOf(-1, 0, 3, 0, 5)

            val expected = -15
            val actual = targetClass.dotProductNonZeroOrNull(indexes, values)

            assertEquals(expected, actual)
        }
    }

    @Nested
    @DisplayName("Тестирование функции findMinOrNull")
    inner class LaboratoryOneFindMinOrNull {
        @Test
        @DisplayName("Возвращает null на пустой массив")
        fun `returns null on empty array`() {
            val data = arrayOf<Int>()

            val expected: Pair<Int, Index>? = null
            val actual = targetClass.findMinOrNull(data)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Обрабатывает массив из одного элемента")
        fun `check array with size = 1`() {
            val data = arrayOf(10)
            val index = Index(0)

            val expected = data[index.value] to index
            val actual = targetClass.findMinOrNull(data)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Обрабатывает массив из двух элементов")
        fun `check array with size = 2`() {
            val data = arrayOf(1, 0, -10)
            val index = Index(data.size - 1)

            val expected = data[index.value] to index
            val actual = targetClass.findMinOrNull(data)

            assertEquals(expected, actual)
        }

        @Test
        @DisplayName("Обрабатывает массив из пяти элементов")
        fun `check array with size = 5`() {
            val data = arrayOf(5, 3, 0, 10, 1)
            val index = Index(2)

            val expected = data[index.value] to index
            val actual = targetClass.findMinOrNull(data)

            assertEquals(expected, actual)
        }
    }

    @TestFactory
    @DisplayName("Тестирование функции reverse")
    fun `reverse function tests`(): Collection<DynamicTest> {
        val data = listOf(
            arrayOf(),
            arrayOf(1f),
            arrayOf(1.5f, 2f),
            arrayOf(5f, 3f, 1.8f, -3f, -5f)
        )

        return data.map { testArray ->
            dynamicTest("Инвертирует массив: [${testArray.joinToString()}]") {
                val expected = testArray.reversedArray()
                targetClass.reverse(testArray)
                assertEquals(expected.joinToString(), testArray.joinToString())
            }
        }.toList()
    }
}